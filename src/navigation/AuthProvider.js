import React, { createContext, useState } from 'react';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';

export const AuthContext = createContext({});

GoogleSignin.configure({
  webClientId: '998103844470-f2r5vjj6bni3hm1hjro7lsudad3ib1t9.apps.googleusercontent.com',
  offlineAccess: true
});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);

    return (
      <AuthContext.Provider
        value={{
          user,
          setUser,
          login: async (email, password) => {
            try {
              await auth().signInWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          register: async (email, password) => {
            try {
              await auth().createUserWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          logout: async () => {
            try {
              await auth().signOut();
              setUser(null);
            } catch (e) {
              console.error(e);
            }
          },
          googleSignIn: async () => {
                  // Get the users ID token
            const { idToken } = await GoogleSignin.signIn();
          
            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);
          
            // Sign-in the user with the credential
            return auth().signInWithCredential(googleCredential);
          }
        }
      }
    >
      {children}
    </AuthContext.Provider>
  );
};
