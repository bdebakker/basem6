import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Alert, Button } from 'react-native';
import { List, Divider, FAB } from 'react-native-paper';
import analytics from '@react-native-firebase/analytics';
import firestore from '@react-native-firebase/firestore';
import Loading from '../components/Loading';
import useStatsBar from '../utils/useStatusBar';
import { AuthContext } from '../navigation/AuthProvider';

export default function HomeScreen({ navigation }) {
  useStatsBar('light-content');

  const [threads, setThreads] = useState([]);
  const [loading, setLoading] = useState(true);
  const { user } = useContext(AuthContext);

  
  /**
   * Fetch threads from Firestore
   */
   useEffect(() => {
    const unsubscribe = firestore()
      .collection('THREADS')
      .orderBy('latestMessage.createdAt', 'desc')
      .onSnapshot(querySnapshot => {
        const threads = querySnapshot.docs.map(documentSnapshot => {
          return {
            _id: documentSnapshot.id,
            // give defaults
            name: '',

            latestMessage: {
              text: ''
            },
            ...documentSnapshot.data()
          };
        });

        setThreads(threads);

        if (loading) {
          setLoading(false);
        }
      });

    /**
     * unsubscribe listener
     */
    return () => unsubscribe();
  }, []);

  async function navigateWithAnalytics(item, navigation){
    await analytics().setUserProperties({
      account_type: 'gold',
      account_name: 'Gold Badge',
    });
  
    await analytics().logEvent('ScreenTrack', {
      id: "012345",
      item: item.name,
      description: ['desc1', 'desc2'],
    }).then(
      navigation.navigate('Room', { thread: item })
    );
  }
  

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={threads}
        keyExtractor={item => item._id}
        ItemSeparatorComponent={() => <Divider />}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={ async () => await navigateWithAnalytics(item, navigation)}>
            <List.Item
              title={item.name}
              description={item.latestMessage.text}
              titleNumberOfLines={1}
              titleStyle={styles.listTitle}
              descriptionStyle={styles.listDescription}
              descriptionNumberOfLines={1}
            />
          </TouchableOpacity>
        )}
      />
      <FAB
        style={styles.fab}
        icon="plus"
        onPress={() => crashFunction(user.uid)}
      />
    </View>
  );
}

function crashFunction(id){
  // crashlytics().log('crash on start component.');
  // crashlytics().setUserId(id);
  // crashlytics().crash()
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1
  },
  listTitle: {
    fontSize: 22
  },
  listDescription: {
    fontSize: 16
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});
